<?php

namespace UniTestor\tests\units{

    require_once(__DIR__.'./../../../Source/Vendor/php/Object/Account_Proposal.php');

    use \atoum;

    class Account_Proposal extends atoum {

        public function testSetAndGetUser()
        {
            $userId = 1;
            $this
                ->given($account_proposal = new \UniTestor\Account_Proposal('','','',''))
                ->when($account_proposal->setFkeyAccount($userId))
                ->then
                ->integer($account_proposal->getFkeyAccount())
                ->isEqualTo($userId);
        }

        public function testSetAndGetProposal()
        {
            $key_proposal = 12;
            $this
                ->given($account_proposal = new \UniTestor\Account_Proposal('','','',''))
                ->when($account_proposal->setFkeyProposal($key_proposal))
                ->then
                ->integer($account_proposal->getFkeyProposal())
                ->isEqualTo($key_proposal);
        }

        public function testSetAndGetReponse()
        {
            $reponse = 0;
            $this
                ->given($account_proposal = new \UniTestor\Account_Proposal('','','',''))
                ->when($account_proposal->setReponse($reponse))
                ->then
                ->integer($account_proposal->getReponse())
                ->isEqualTo($reponse);
        }
    }

}
