<?php




namespace UniTestor\tests\units {
    require_once(__DIR__.'./../../../Source/Vendor/php/Object/Account.php');
    use \atoum;

    class Account extends atoum
    {
        public function testConstructor()
        {
            $Username = "Corentin";
            $Password = "12345";

            $this
            ->if($account = new \UniTestor\Account($Username,'', $Password,'','',''))
            ->then
                ->String($account->getUsername())->isEqualTo($Username)
                ->String($account->getPassword())->isEqualTo($Password)
            ;

        }
        public function testFullConstructor()
        {
            $Username = "UserTest";
            $Email = "UserTest1@mail.com";
            $Password = "12345";
            $Name = "Jack";
            $Surname = "Smith";
            $Bday = "1995/08/04";
            $this
            ->if($account = new \UniTestor\Account($Username,$Email,$Password,$Name,$Surname,$Bday))
            ->then
                ->String($account->getUsername())->isEqualTo($Username)
                ->String($account->getPassword())->isEqualTo($Password)
                ->String($account->getEmail())->isEqualTo($Email)
                ->String($account->getName())->isEqualTo($Name)
                ->String($account->getSurname())->isEqualTo($Surname)
                ->String($account->getBirthday())->isEqualTo($Bday)
            ;

        }


    }
}
