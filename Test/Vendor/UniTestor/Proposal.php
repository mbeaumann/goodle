<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 23/10/2018
 * Time: 14:25
 */

namespace UniTestor\tests\units {

    require_once(__DIR__ . './../../../Source/Vendor/php/Object/Proposal.php');

    use \atoum;


    class Proposal extends atoum
    {
        public function testSetAndGetEventId()
        {
            $eventId = 3;
            $this
                ->given($proposal = new \UniTestor\Proposal('',''))
                ->when($proposal->setFkeyEvent($eventId))
                ->then
                ->integer($proposal->getFkeyEvent())
                ->isEqualTo($eventId);
        }

        public function testHydrateProposal()
        {
            $data = [
                'Id' => 1,
                'Date' => '1999-08-05',
                'Votes' => 133,
                'fkey_event' => 12,
            ];
            $this
                ->given($proposal = new \UniTestor\Proposal('',''))
                ->when($proposal->hydrate($data))
                ->then
                ->String($proposal->getDate())
                ->isEqualTo($data['Date']);
        }

        public function testGetAndSetVote()
        {
            $vote = 233;
            $this
                ->given($proposal = new \UniTestor\Proposal('',$vote))
                ->then
                ->integer($proposal->getVotes())
                ->isEqualTo($vote);
        }

    }
}