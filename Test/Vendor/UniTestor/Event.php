<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 09/10/2018
 * Time: 20:58
 */

namespace UniTestor\tests\units {

    require_once(__DIR__ . './../../../Source/Vendor/php/Object/Event.php');

    use \atoum;

    class Event extends atoum
    {

        public function testHydrate()
        {
            $data = [
                'Id' => 1,
                'Owner' => 1,
                'Name' => 'Le Beau Event',
                'Description' => 'Un event avec de l\'alcool',
                'fkey_account' => 1,
                'fkey_query' => 2,
            ];
            $this
                ->given($event = new \UniTestor\Event('','','',''))
                ->when($event->hydrate($data))
                ->then
                ->integer($event->getId())
                ->isEqualTo($data['Id']);
        }

        public function testConstructor() {

            $Owner = 1;
            $nameEvent = "Le beau event";
            $Description = "12345";
            $finSondage = "Jack";
            $this
                ->if($event = new \UniTestor\Event($Owner,$nameEvent,$Description,$finSondage))
                ->then
                ->Integer($event->getOwner())->isEqualTo($Owner)
                ->String($event->getName())->isEqualTo($nameEvent)
                ->String($event->getDescription())->isEqualTo($Description)
                ->String($event->getFinSondage())->isEqualTo($finSondage)
            ;

        }



    }
}