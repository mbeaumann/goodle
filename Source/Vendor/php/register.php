<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
  <title>Goodle - Register</title>
  <!--<link rel="stylesheet" type="text/css" href="../styles/general.css">-->
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <h2>Formulaire d'inscription</h2> 
    
        <form method="post" action="register.php">
            <?php include('errors.php'); ?>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label> Identifiant:</label>
                    <input type="text" class="form-control" name="username" value="<?php echo $username; ?>" required>
                </div>
                <div class="col-sm-6 form-group">
                    <label> Email:</label>
                    <input type="email" class="form-control" name="email" value="<?php echo $email; ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label> Mot de passe:</label>
                    <input type="password" class="form-control" name="password_1" required>
                </div>
                <div class="col-sm-6 form-group">
                    <label> Ressaisir mot de passe:</label>
                    <input type="password" class="form-control" name="password_2" required>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label> Prénom:</label>
                    <input type="text" class="form-control" name="name" value="<?php echo $name; ?>" required>
                </div>
                <div class="col-sm-6 form-group">
                    <label> Nom:</label>
                    <input type="text" class="form-control" name="surname" value="<?php echo $surname; ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label> Date de naissance:</label>
                    <input id="bday" type="text" class="form-control" name="bday" maxlength="10" onfocus="this.value=''" value="<?php echo "YYYY-MM-DD"  ?>" required>
                </div>
            </div>
            <div class="row">
                
            </div>
            <div class="row">
                <div class="col-sm-12 form-group">
                    <button type="submit" class="btn btn-lg btn-success btn-block" name="reg_user">Soumettre </button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <p>Déjà membre ? <a href="connexion.php">Connexion</a></p>
                </div>
            </div>
        </form>

        <div id="success_message" style="width:100%; height:100%; display:none; "> <h3>Inscription validé !</h3> </div>
        <div id="error_message" style="width:100%; height:100%; display:none; "> <h3>Erreur</h3> Désolé il y a des erreurs dans votre formulaire. </div>
      </div>
    </div>
  </div>
</body>
</html>

