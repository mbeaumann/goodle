<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 09/10/2018
 * Time: 13:18
 */



class EventManager
{

    private $_db; // Instance de PDO.

    public function __construct($db)
    {
        $this->setDb($db);
    }

    /**
     * Function that try to register a user
     */

    public function add($event)
    {
        try{
            $name = $event->getName();
            $description = $event->getDescription();
            $owner = $event->getOwner();
            $finSondage = $event->getFinSondage();


            $insert_query = "INSERT INTO EVENT (Name, Description, Owner, FinSondage) 
                VALUES('$name', '$description', '$owner', '$finSondage')";
            $request = $this->_db->query($insert_query);

            return $this->_db->lastInsertId();
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }

    }


    public function delete()
    {

    }

    public function get($Id)
    {
        $S = "SELECT * FROM EVENT WHERE Id = '$Id'";
        $R = $this->_db->query($S);

        return $R;
    }

    public function getList()
    {

    }

    public function update($user)
    {

    }

    public function setDb(PDO $db)
    {
        $this->_db = $db;
    }

    /**
     * Function that return the event of a owner
     */
    public function getEventOwner($Id) {

        $S = "SELECT * FROM EVENT WHERE Owner = '$Id'";
            $R = $this->_db->query($S);

            return $R;

    }
    
    /**
     * Function that return the event (chose on the list) of a owner
     */
    public function getEventIdList($idEvent, $idOwner) {
        $S = "SELECT * FROM EVENT WHERE Owner = '$idOwner' AND Id = '$idEvent'";
        $R = $this->_db->query($S);
        
        return $R;
    }
    
    /**
     * Function that return an event (chose on the list) and the proposals of the event
     */
    public function getEventAndProposal($idEvent, $idOwner) {
        $S = "SELECT * FROM EVENT
                    INNER JOIN PROPOSAL ON EVENT.Id = PROPOSAL.fkey_event
                    WHERE EVENT.Owner = '$idOwner' AND EVENT.Id = '$idEvent'";
        $R = $this->_db->query($S);
        
        return $R;
    }


    public function getEventFromProposal($IdAccount) {
        $S = "SELECT DISTINCT EVENT.Id, Name, Description, FinSondage FROM EVENT, PROPOSAL, ACCOUNT_PROPOSAL WHERE EVENT.Id = PROPOSAL.fkey_event
				AND PROPOSAL.Id = ACCOUNT_PROPOSAL.fkey_proposal
                AND ACCOUNT_PROPOSAL.fkey_account = '$IdAccount'";
        $R = $this->_db->query($S);

        return $R;
    }
    
    public function getEventFromProposalView($IdAccount) {
        $S = "SELECT * FROM EVENT, PROPOSAL, ACCOUNT_PROPOSAL WHERE EVENT.Id = PROPOSAL.fkey_event
				AND PROPOSAL.Id = ACCOUNT_PROPOSAL.fkey_proposal
                AND ACCOUNT_PROPOSAL.fkey_account = '$IdAccount'";
        $R = $this->_db->query($S);
        
        return $R;
    }
    
    /*
     * Function that delete an event (after clic on the suppression button)
     */
    public function deleteEvent($idEvent) {
        $S = "DELETE FROM EVENT WHERE Id = '$idEvent'";
        $R = $this->_db->query($S);
        
        return $R;
    }
    
    /*
     * Function that associate the date with the event (after that the creator of the event chose a date)
     */
    public function chooseEventDate($idAccount, $idEvent, $response) {
        $S = "UPDATE EVENT
                    SET DateChoisie = '$response'
                    WHERE Id = '$idEvent'
                    AND Owner = '$idAccount'";
        $R = $this->_db->query($S);
        
        return $R;
    }
}
