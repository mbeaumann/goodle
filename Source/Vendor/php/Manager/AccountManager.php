<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 09/10/2018
 * Time: 13:18
 */



class AccountManager
{

    private $_db; // Instance de PDO.

    public function __construct($db)
    {
        $this->setDb($db);
    }

    /**
     * Function that try to register a user
     */

    public function add($user)
    {
        try{
            $username = $user->getUsername();
            $email = $user->getEmail();
            $password = $user->getPassword();
            $name = $user->getName();
            $surname = $user->getSurname();
            $bday = $user->getBirthday();

            $insert_query = "INSERT INTO ACCOUNT (Username, Email, Password, Name, Surname, Birthdate) 
                VALUES('$username', '$email', '$password', '$name', '$surname', '$bday')";
            $request = $this->_db->query($insert_query);
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }

    }


    public function delete()
    {

    }

    public function get($Id)
    {
        $S = "SELECT * FROM ACCOUNT WHERE Id = '$Id'";
        $R = $this->_db->query($S);

        return $R;
    }

    public function getList()
    {

    }

    public function update($user)
    {
        try {

            $username = $user->getUsername();
            $email = $user->getEmail();
            $password = $user->getPassword();
            $name = $user->getName();
            $surname = $user->getSurname();
            $bday = $user->getBirthday();
            $id = $user->getId();


            $request = $this->_db->prepare("UPDATE ACCOUNT SET 
						Email = '$email',
						Password = '$password'
					WHERE Id = '$id'");
            $request->execute();


        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
    }

    public function setDb(PDO $db)
    {
        $this->_db = $db;
    }


    /**
     * Function that try to login an user
     */
    public function login($user) {
        $username = $user->getUsername();
        $pass = $user->getPassword();
        $email = $user->getEmail();

        $S = "SELECT * FROM ACCOUNT WHERE (Username = '$username' OR Email = '$email') AND Password = '$pass'";

        $R = $this->_db->query($S);
        $D = $R->fetch();

        $Id = -1;
        if ($R->rowCount() > 0) {
            $Id = $D['Id'];
        }


        return $Id;
    }


    /**
     * Function that check if the user already exist
     */
    public function user_check($user){

        $username = $user->getUsername();
        $user_check_query = "SELECT * FROM ACCOUNT WHERE Username='$username'";
        $result = $this->_db->query($user_check_query);
        $count = $result->rowCount();
        return $count;
    }
    /**
     * Function that check if the email already exist
     */
    public function email_check($user){
        $email = $user->getEmail();
        $email_check_query = "SELECT * FROM ACCOUNT WHERE Email='$email'";
        $result = $this->_db->query($email_check_query);
        $count = $result->rowCount();
        return $count;
    }

    /**
     * Function that verify the password of user in order to change info
     */
    public function verifyPass($user, $pass) {
        $username = $user->getUsername();

        $S = "SELECT * FROM ACCOUNT WHERE Username = '$username' AND Password = '$pass'";

        $R = $this->_db->query($S);
        $D = $R->fetch();

        $res = -1;
        if ($R->rowCount() > 0) {
            $Id = $D['Id'];
        }


        return $res;
    }

}
