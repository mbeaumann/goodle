<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 16/10/2018
 * Time: 13:12
 */

class ProposalManager
{

    private $_db; // Instance de PDO.

    public function __construct($db)
    {
        $this->setDb($db);
    }

    public function add($proposal)
    {
        try{
            $Date = $proposal->getDate();
            $fkey_event = $proposal->getFkeyEvent();

            $insert_query = "INSERT INTO PROPOSAL (Date, fkey_event) 
                VALUES('$Date', '$fkey_event')";
            $request = $this->_db->query($insert_query);
            $idProposal = $this->_db->lastInsertId();
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
        return $idProposal;

    }


    public function delete()
    {

    }

    public function get()
    {

    }

    public function getList()
    {

    }

    public function update()
    {

    }

    public function setDb(PDO $db)
    {
        $this->_db = $db;
    }
    
    /*
     * Function that delete the proposals of an event (after clic on the suppression button)
     */
    public function deleteProposal($idEvent) {
        $S = "DELETE FROM PROPOSAL WHERE fkey_event = '$idEvent'";
        $R = $this->_db->query($S);
        
        return $R;
    }
}