<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 16/10/2018
 * Time: 13:13
 */

class Account_ProposalManager
{

    private $_db; // Instance de PDO.

    public function __construct($db)
    {
        $this->setDb($db);
    }

    public function add($query)
    {
        /*try{
            $Owner = $query->getOwner();
            $Name = $query->getName();


            $insert_query = "INSERT INTO QUERY (Owner, Name, Description, finSondage) 
                VALUES('$Owner', '$Name', '$Description', '$finSondage')";
            $request = $this->_db->query($insert_query);
            $idEvent = $this->_db->lastInsertId();
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
        return $idEvent;*/

    }


    public function delete()
    {

    }

    public function get()
    {

    }

    public function getList()
    {

    }

    public function update($account_proposal)
    {
        try {

            $userId = $account_proposal->getFkeyAccount();
            $proposalId = $account_proposal->getFkeyProposal();
            $reponse = $account_proposal->getReponse();


            $request = $this->_db->prepare("UPDATE ACCOUNT_PROPOSAL SET 
						Reponse = '$reponse'
					WHERE fkey_account = '$userId' AND fkey_proposal = '$proposalId'");
            $request->execute();


        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
    }

    public function setDb(PDO $db)
    {
        $this->_db = $db;
    }
    
    /*
     * Function that delete the proposals of an account (after clic on the suppression button)
     */
    public function deleteAccountProposal($idEvent) {
        $S = "DELETE FROM ACCOUNT_PROPOSAL
                INNER JOIN PROPOSAL ON ACCOUNT_PROPOSAL.fkey_proposal = PROPOSAL.Id
                WHERE PROPOSAL.fkey_event = '$idEvent'";
        $R = $this->_db->query($S);
        
        return $R;
    }
}