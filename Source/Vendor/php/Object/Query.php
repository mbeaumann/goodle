<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 05/10/2018
 * Time: 18:38
 */

class Query
{
    private $Id;
    private $fkey_event;
    private $fkey_proposal;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param mixed $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return mixed
     */
    public function getFkeyEvent()
    {
        return $this->fkey_event;
    }

    /**
     * @param mixed $fkey_event
     */
    public function setFkeyEvent($fkey_event)
    {
        $this->fkey_event = $fkey_event;
    }

    /**
     * @return mixed
     */
    public function getFkeyProposal()
    {
        return $this->fkey_proposal;
    }

    /**
     * @param mixed $fkey_proposal
     */
    public function setFkeyProposal($fkey_proposal)
    {
        $this->fkey_proposal = $fkey_proposal;
    }

}