<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 05/10/2018
 * Time: 18:40
 */

namespace UniTestor {

    class Proposal
    {
        private $Id;
        private $Date;
        private $Votes;
        private $fkey_event;

        public function __construct($Date, $Votes)
        {
            $this->Date = $Date;
            $this->Votes = $Votes;
        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->Id;
        }

        /**
         * @param mixed $Id
         */
        public function setId($Id)
        {
            $this->Id = $Id;
        }

        /**
         * @return mixed
         */
        public function getDate()
        {
            return $this->Date;
        }

        /**
         * @param mixed $Date
         */
        public function setDate($Date)
        {
            $this->Date = $Date;
        }

        /**
         * @return mixed
         */
        public function getVotes()
        {
            return $this->Votes;
        }

        /**
         * @param mixed $Votes
         */
        public function setVotes($Votes)
        {
            $this->Votes = $Votes;
        }

        /**
         * @return mixed
         */
        public function getFkeyEvent()
        {
            return $this->fkey_event;
        }

        /**
         * @param mixed $fkey_event
         */
        public function setFkeyEvent($fkey_event)
        {
            $this->fkey_event = $fkey_event;
        }

        public function hydrate(array $donnees)
        {
            foreach ($donnees as $key => $value) {
                $method = 'set' . ucfirst($key);
                
                if (method_exists($this, $method)) {
                    $this->$method($value);
                }
            }
        }
    }
}