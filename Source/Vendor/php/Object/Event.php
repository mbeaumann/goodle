<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 05/10/2018
 * Time: 18:42
 */

namespace UniTestor {


    class Event
    {
        private $Id;
        private $Owner;
        private $Name;
        private $Description;
        private $fkey_account;
        private $fkey_query;
        private $FinSondage;




        public function __construct($Owner, $Name, $Description, $FinSondage)
        {
            $this->Owner = $Owner;
            $this->Name = $Name;
            $this->Description = $Description;
            $this->FinSondage = $FinSondage;
        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->Id;
        }

        /**
         * @param mixed $Id
         */
        public function setId($Id)
        {
            $this->Id = $Id;
        }

        /**
         * @return mixed
         */
        public function getOwner()
        {
            return $this->Owner;
        }

        /**
         * @param mixed $Owner
         */
        public function setOwner($Owner)
        {
            $this->Owner = $Owner;
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->Name;
        }

        /**
         * @return mixed
         */
        public function getDescription()
        {
            return $this->Description;
        }

        /**
         * @param mixed $Description
         */
        public function setDescription($Description)
        {
            $this->Description = $Description;
        }

        /**
         * @param mixed $Name
         */
        public function setName($Name)
        {
            $this->Name = $Name;
        }

        /**
         * @return mixed
         */
        public function getFkeyAccount()
        {
            return $this->fkey_account;
        }

        /**
         * @param mixed $fkey_account
         */
        public function setFkeyAccount($fkey_account)
        {
            $this->fkey_account = $fkey_account;
        }

        /**
         * @return mixed
         */
        public function getFkeyQuery()
        {
            return $this->fkey_query;
        }

        /**
         * @param mixed $fkey_query
         */
        public function setFkeyQuery($fkey_query)
        {
            $this->fkey_query = $fkey_query;
        }

        /**
         * @return mixed
         */
        public function getFinSondage()
        {
            return $this->FinSondage;
        }

        /**
         * @param mixed $FinSondage
         */
        public function setFinSondage($FinSondage)
        {
            $this->FinSondage = $FinSondage;
        }

        public function hydrate(array $donnees)
        {
            foreach ($donnees as $key => $value) {
                $method = 'set' . ucfirst($key);

                if (method_exists($this, $method)) {
                    $this->$method($value);
                }
            }
        }
    }
}