<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 04/10/2018
 * Time: 16:07
 */


namespace UniTestor {

    class Account
    {
        private $Id;
        private $Username;
        private $Email;
        private $Password;
        private $isAdmin;
        private $fkey_event;
        private $Name;
        private $Surname;
        private $Birthday;


        // Constructor
        /*public function __construct($Username, $Password)
        {
            $this->Username = $Username;
            $this->Password = $Password;
        }*/

        public function __construct($Username, $Email, $Password, $Name, $Surname, $Birthday)
        {
            $this->Username = $Username;
            $this->Email = $Email;
            $this->Password = $Password;
            $this->Name = $Name;
            $this->Surname = $Surname;
            $this->Birthday = $Birthday;
        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->Id;
        }

        /**
         * @param mixed $Id
         */
        public function setId($Id)
        {
            $this->Id = $Id;
        }

        /**
         * @return mixed
         */
        public function getUsername()
        {
            return $this->Username;
        }

        /**
         * @param mixed $Username
         */
        public function setUsername($Username)
        {
            $this->Username = $Username;
        }

        /**
         * @return mixed
         */
        public function getEmail()
        {
            return $this->Email;
        }

        /**
         * @param mixed $Email
         */
        public function setEmail($Email)
        {
            $this->Email = $Email;
        }

        /**
         * @return mixed
         */
        public function getPassword()
        {
            return $this->Password;
        }

        /**
         * @param mixed $Password
         */
        public function setPassword($Password)
        {
            $this->Password = $Password;
        }

        /**
         * @return mixed
         */
        public function getisAdmin()
        {
            return $this->isAdmin;
        }

        /**
         * @param mixed $isAdmin
         */
        public function setIsAdmin($isAdmin)
        {
            $this->isAdmin = $isAdmin;
        }

        /**
         * @return mixed
         */
        public function getFkeyEvent()
        {
            return $this->fkey_event;
        }

        /**
         * @param mixed $fkey_event
         */
        public function setFkeyEvent($fkey_event)
        {
            $this->fkey_event = $fkey_event;
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->Name;
        }

        /**
         * @param mixed $Name
         */
        public function setName($Name)
        {
            $this->Name = $Name;
        }

        /**
         * @return mixed
         */
        public function getSurname()
        {
            return $this->Surname;
        }

        /**
         * @param mixed $Surname
         */
        public function setSurname($Surname)
        {
            $this->Surname = $Surname;
        }

        /**
         * @return mixed
         */
        public function getBirthday()
        {
            return $this->Birthday;
        }

        /**
         * @param mixed $Birthday
         */
        public function setBirthday($Birthday)
        {
            $this->Birthday = $Birthday;
        }


    }
}