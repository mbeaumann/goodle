<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 05/10/2018
 * Time: 18:43
 */

namespace UniTestor {

    class Account_Proposal
    {
        private $Id;
        private $fkey_account;
        private $fkey_proposal;
        private $Reponse;

        public function __construct()
        {

        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->Id;
        }

        /**
         * @param mixed $Id
         */
        public function setId($Id)
        {
            $this->Id = $Id;
        }

        /**
         * @return mixed
         */
        public function getFkeyAccount()
        {
            return $this->fkey_account;
        }

        /**
         * @param mixed $fkey_account
         */
        public function setFkeyAccount($fkey_account)
        {
            $this->fkey_account = $fkey_account;
        }

        /**
         * @return mixed
         */
        public function getFkeyProposal()
        {
            return $this->fkey_proposal;
        }

        /**
         * @param mixed $fkey_proposal
         */
        public function setFkeyProposal($fkey_proposal)
        {
            $this->fkey_proposal = $fkey_proposal;
        }

        /**
         * @return mixed
         */
        public function getReponse()
        {
            return $this->Reponse;
        }

        /**
         * @param mixed $Reponse
         */
        public function setReponse($Reponse)
        {
            $this->Reponse = $Reponse;
        }

    }
}