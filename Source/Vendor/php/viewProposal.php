<?php

ob_start();

require("./DB/Database.php");
require("./Object/Account.php");
require("./Object/Event.php");
require("./Object/Proposal.php");
require("./Object/Account_Proposal.php");
require("./Manager/AccountManager.php");
require("./Manager/EventManager.php");
require("./Manager/Account_ProposalManager.php");
require("./bibli_cuiteur.php");

$title = "Goodle - Event";
$style = "./../styles/test.css";

session_start();

if(!isset($_SESSION['Id'])) {
    header('location: ../../welcome.php');
    exit();
}

$Id = $_SESSION['Id'];
$Name = $_SESSION['Name'];
$Username = $_SESSION['Username'];
$Email = $_SESSION['Email'];
$Surname = $_SESSION['Surname'];
$Password = $_SESSION['Password'];

gk_cb_html_debut($title, $style);
affiche_nav_connected();

use \UniTestor as UT;

$db = new Database();

$user = new UT\Account($Username, $Email, $Password, $Name, $Surname,'');
$user->setId($Id);
$event_manager = new EventManager($db->getConnection());

view_event($Id, $event_manager, $_GET['id']);


if (isset($_POST['btnChoisir'])) {

    foreach($_POST['answer'] as $key=>$value){
        $account_proposal = new UT\Account_Proposal();
        $account_proposal->setFkeyAccount($user->getId());
        $account_proposal->setFkeyProposal($key);
        if ($value == "oui") {
            $account_proposal->setReponse(1);
        }
        else if ($value == "non") {
            $account_proposal->setReponse(0);
        }
        else if ($value == "p-e") {
            $account_proposal->setReponse(2);
        }

        $account_proposal_manager = new Account_ProposalManager($db->getConnection());
        $account_proposal_manager->update($account_proposal);
    }
}




function view_event($userId, $event_manager, $eventId) {
    //$result = $event_manager->getEventAndProposal($eventId, $userId);
    $result = $event_manager->getEventFromProposal($userId);
    $row = $result->fetch(PDO::FETCH_ASSOC);
    $event = new UT\Event('','','','');
    if($result->rowCount() == 0) {
        header("location: ../../welcome.php");
        exit();
    }

    //$resultAll = $event_manager->getEventAndProposal($eventId, $userId);
    $resultAll = $event_manager->getEventFromProposalView($userId);
    $rowAll = $resultAll->fetchAll(PDO::FETCH_ASSOC);
    $proposal = new UT\Proposal('','');
    if($resultAll->rowCount() == 0) {
        header("location: ../../welcome.php");
        exit();
    }

    $event->hydrate($row);

    echo    '<div class="container">
                <h2>Evénement - '.$event->getName().'</h2>';
    echo            '<p>Description : '.$event->getDescription().'</p>';
    echo            '<p>Fin du sondage : '.$event->getFinSondage().'</p>';
    echo            '<p>Proposition :</p>';
    echo '<form method="post">';
    foreach($rowAll as $r) {
        $proposal->hydrate($r);
        echo '<h4>'.$proposal->getDate().'</h4>';
            echo "<input type='radio' name='answer[" . $proposal->getId(). "]' value='oui'> Oui </input>
                <input type='radio' name='answer[".$proposal->getId()."]' value='non'> Non </input>
                <input type='radio' name='answer[".$proposal->getId()."]' value='p-e'> Peut-être </input>";
                echo '<br>';
                echo '<br>';
            }
            
    echo '<input type="submit" name="btnChoisir">
    </form>';


    
    echo    '</div>';
}

$bd = null;
gk_cb_html_fin() ;

?>