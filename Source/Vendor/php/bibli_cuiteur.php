<?php 

	/**
	* Permet de rendre une date claire
	*
	* Fonction qui prend en paramètre un integer 
	* et qui retourne la date dans un format plus jolie
	* exemple : "20140224" donnera "24 Fevrier 2014"
	* 
	* @param	integer		$data 	Date au format AAAAMMDD

	*
	* @return 	string 	$date	Valeur du paramètre ou FALSE
	*/
		
	function gk_cb_amj_clair($data) {
		$annee = (int) substr($data, 0, 4);
		$mois = (int) substr($data, 4, 2);
		$jour = (int) substr($data, 6, 2);
			
		$ma = array('Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre');
			
		$date = $jour. ' '. $ma[$mois-1]. ' '. $annee;
		return $date;
	}
	
	/**
	* Ecrit les premières lignes nécessaires pour le code html
	*
	* Cette fonction va nous permettre de ne pas 
	* écrire à chaque fois les lignes de code html 
	* en pouvant choisir le titre et le chemin du fichier CSS
	*
	* @param	string 		$title		Titre de la page affiché en haut 		
	* @param 	string		$style		Chemin vers le fichier CSS
	*
	*/
	
	function gk_cb_html_debut($title, $style) {
		echo '
			<!DOCTYPE html>
				<html lang="en">
				<head>
					<title>',$title,'</title>
					<!--<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1">
                    <link rel="stylesheet" href="">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
					<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
				</head>';
	}

	function affiche_nav() {
		echo '<nav class="navbar navbar-inverse">
						<div class="container-fluid">
							<div class="navbar-header">
								<a class="navbar-brand">Goodle</a>
							</div>
							<ul class="nav navbar-nav">
								<li class="active"><a href="accueil.php">Accueil</a></li>
								<li><a href="createEvent.php">Cr&eacute;er &eacute;v&eacute;nement</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><a href="#"><span class="glyphicon glyphicon-user"></span> Voir profil</a></li>
								<li><a href="register.php"><span class="glyphicon glyphicon-user"></span> S\'inscrire</a></li>
								<li><a href="connexion.php"><span class="glyphicon glyphicon-log-in"></span> Connexion</a></li>
								<li><a href="#"><span class="glyphicon glyphicon-log-out"></span> D&eacute;connexion</a></li>
							</ul>
						</div>
					</nav>';
	}

	/**
	*Affichage de la barre de navigation lorsque l'utilisateur est connect�
	*/
	function affiche_nav_connected() {
		echo '<nav class="navbar navbar-inverse">
						<div class="container-fluid">
							<div class="navbar-header">
								<a class="navbar-brand">Goodle</a>
							</div>
							<ul class="nav navbar-nav">
								<li class="active"><a href="accueil.php">Accueil</a></li>
								<li><a href="createEvent.php">Cr&eacute;er &eacute;v&eacute;nement</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><a href="compte.php"><span class="glyphicon glyphicon-user"></span> ';echo $_SESSION['Username'];echo'</a></li>
								<li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> D&eacute;connexion</a></li>
							</ul>
						</div>
					</nav>';
	}

	/**
	*Affichage de la barre de navigation lorsque l'utilisateur n'est pas connect�
	*/
	function affiche_nav_no_connected() {
		echo '<nav class="navbar navbar-inverse">
						<div class="container-fluid">
							<div class="navbar-header">
								<a class="navbar-brand">Goodle</a>
							</div>
							<ul class="nav navbar-nav">
								<li class="active"><a href="accueil.php">Accueil</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><a href="register.php"><span class="glyphicon glyphicon-user"></span> S\'inscrire</a></li>
								<li><a href="connexion.php"><span class="glyphicon glyphicon-log-in"></span> Connexion</a></li>
							</ul>
						</div>
					</nav>';
	}

	/**
	* Ecrit les dernières lignes de code html
	*
	* Cette fonction va nous permettre de ne pas 
	* écrire les lignes de code html à chaque fois
	*
	*
	*/
		
	function gk_cb_html_fin() {
		echo '</html>';
	}
	
	/**
	 * Connexion à la base de données.
	 *
	 * @return resource	connecteur à la base de données
	 */
	 
	function gk_cb_bd_connection() {
        $bd = mysqli_connect('m2gl.deptinfo-st.univ-fcomte.fr', 'm2test3', 'm2test3', 'm2test3');

        if ($bd !== FALSE) {
			return $bd;     // Sortie connexion OK
	  	}

		// Erreur de connexion
		// Collecte des informations facilitant le debugage
		$msg = '<h4>Erreur de connexion base MySQL</h4>'
		.'<div style="margin: 20px auto; width: 350px;">'
			.'BD_SERVEUR : '.BD_SERVEUR
			.'<br>BD_USER : '.BD_USER
			.'<br>BD_PASS : '.BD_PASS
			.'<br>BD_NOM : '.BD_NOM
			.'<p>Erreur MySQL num�ro : '.mysqli_connect_errno($bd)
			.'<br>'.mysqli_connect_error($bd)
		.'</div>';

		cb_bd_ErreurExit($msg);
	}
	

	/**
	 * Gestion d'une erreur de requ�te base de donn�es.
	 *
	 * @param	resource	$bd		Connecteur sur la bd ouverte
	 * @param	string		$sql	requ�te SQL provoquant l'erreur
	 */
	 	
	function gk_cb_bd_erreur($bd, $sql) {
		$errNum = mysqli_errno($bd);
		$errTxt = mysqli_error($bd);

		// Collecte des informations facilitant le debugage
		$msg = '<h4>Erreur de requ�te</h4>'
			."<pre><b>Erreur mysql :</b> $errNum"
			."<br> $errTxt"
			."<br><br><b>Requ�te :</b><br> $sql"
			.'<br><br><b>Pile des appels de fonction</b>';

		// R�cup�ration de la pile des appels de fonction
		$msg .= '<table border="1" cellspacing="0" cellpadding="2">'
			.'<tr><td>Fonction</td><td>Appelée ligne</td>'
			.'<td>Fichier</td></tr>';

		$appels = debug_backtrace();
		for ($i = 0, $iMax = count($appels); $i < $iMax; $i++) {
			$msg .= '<tr align="center"><td>'
				.$appels[$i]['function'].'</td><td>'
				.$appels[$i]['line'].'</td><td>'
				.$appels[$i]['file'].'</td></tr>';
		}

		$msg .= '</table></pre>';

		gk_cb_bd_erreurExit($msg);
	}
	
	/**
	 * Arr�t du script si erreur base de données.
	 * Affichage d'un message d'erreur si on est en phase de
	 * d�veloppement, sinon stockage dans un fichier log.
	 *
	 * @param string	$msg	Message affich� ou stocké.
	 */
	 
	function gk_cb_bd_erreurExit($msg) {
		ob_end_clean();		// Supression de tout ce qui
						// a pu �tre d�j� g�n�r�

		echo '<!DOCTYPE html><html><head><meta charset="ISO-8859-1"><title>',
				'Erreur base de données</title></head><body>',
				$msg,
				'</body></html>';
		exit();
	}
	


	/**
	*Cette fonction � l'aide preg_match_all choisi tous les tags.
	*#\D on dit que apr�s # il faut pas avoir les valeurs numeriques, 
	*&*#*[0-9]*; pour chopisir les charactere speciaux, w* 0 ou plusieur char
	*
	* @param	string 		$str			le texte blablas
	*@return	array			$matches		les tags
	*/
	
	function gk_cb_tag($str){
		preg_match_all('/#\D(\w*&*#*[0-9]*;*)*\w*/', $str, $matches, PREG_SET_ORDER);
		return $matches;
	}
	
	/**
	*Cette fonction � l'aide preg_match_all choisi tous les utilisateurs
	*le mot commence @, (.+?) 1 ou plusieur char
	*
	* @param	string 		$str			le texte blablas
	*@return	array			$matches		les utilisateurs mentionn�s
	*/

	function gk_cb_dist($str){
		preg_match_all('/@\w*/', $str, $matches, PREG_SET_ORDER);
		return $matches;
	}
	
	/**
	* Vérification d'une session. Redirection sur la page
	* d'inscription si la session n'est pas ouverte.
	*
	*/	

	function gk_cb_verifie_session(){ 
		if($_SESSION['id']==0 || $_SESSION['pseudo']==NULL){
			header ('location: inscription.php');
			exit();
		}
	}
	





	


	

	

	

	

	
	/**
	 * Cette fonction va faire un cryptage du string donn� 
	 * en param�tre
	 *
	 * @param	string		$string			La chaine de caract�re � crypter
	 *
	 * @return	string		$result			La chaine de caract�re d�crypter
	 *
	 */
	
	function encrypt_url($string) {
		$key = "GUZAL LA MEILLEURE"; //key to encrypt and decrypts.
		$result = '';
		$test = "";
		
		for($i=0; $i<strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));

			$test[$char]= ord($char)+ord($keychar);	
			$result.=$char;
		}

		return urlencode(base64_encode($result));
	}

	/**
	 * Cette fonction va faire un d�cryptage du string donn� 
	 * en param�tre
	 *
	 * @param	string		$string			La chaine de caract�re d�crypter
	 *
	 * @return	string		$result			La chaine de caract�re crypter
	 *
	 */
	 
	function decrypt_url($string) {
		$key = "GUZAL LA MEILLEURE"; //key to encrypt and decrypts.
		$result = '';
		$string = base64_decode(urldecode($string));
		
		for($i=0; $i<strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
	   }
	   return $result;
	}
	
	
	
?>
