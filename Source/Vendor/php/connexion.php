<?php
/**
 * Created by PhpStorm.
 * User: coren
 * Date: 26/09/2018
 * Time: 19:04
 */

ob_start();

require("./DB/Database.php");
require("./Object/Account.php");
require("./Manager/AccountManager.php");
require("./bibli_cuiteur.php");

use \UniTestor as UT;

$title = "Goodle - Connexion";
$style = "../styles/general.css";


gk_cb_html_debut($title, $style);
affiche_nav_no_connected();

if(!isset($_POST['btnConnexion'])) {
    aff_form();
}

$db = new Database();
$db->getConnection()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->getConnection()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
$db->getConnection()->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

if(isset($_POST['btnConnexion'])) {
    $pseudo = trim($_POST['identiant']);
    $pass = md5(trim($_POST['mdp']));

    $user = new UT\Account($pseudo, $pseudo, $pass, '','','');
    $user_manager = new AccountManager($db->getConnection());

        $Id = $user_manager->login($user);

        $erreur = array();
        if($Id == -1) {

            $erreur[] = "User or password invalid";
        }

        if (count($erreur) == 0) {
            $D = $user_manager->get($Id)->fetch();
            session_start();

            $_SESSION['Id'] = $D['Id'];
            $_SESSION['Username'] = $D['Username'];
            $_SESSION['Name'] = $D['Name'];
            $_SESSION['Email'] = $D['Email'];
            $_SESSION['Surname'] = $D['Surname'];
            $_SESSION['Password'] = $D['Password'];


            header ('location: accueil.php');
            exit();
        }

        else {

            echo '<div id="error">',
                    '<p>', $erreur[0], '</p>',
                '</div>';

            $_POST['identiant'] = '';
            $_POST['mdp'] = '';

            echo '</br>';
            echo aff_form();

        }

}




/**
 * La fonction qui va nous afficher le formulaire
 * sous forme de tableau
 */

function aff_form() {

    echo '<body >
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2>Formulaire de connexion</h2> 
                    <form role="form" method="post" id="reused_form">
                    <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="identiant"> Identifiant:</label>
                                <input type="text" class="form-control" id="identiant" name="identiant" maxlength="50">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="mdp"> Mot de passe:</label>
                                <input type="password" class="form-control" id="mdp" name="mdp" maxlength="50">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <button type="submit" class="btn btn-lg btn-success btn-block" id="btnConnexion" name="btnConnexion">Connexion </button>
                            </div>
                        </div>
			            <div class="row">
                            <div class="col-sm-6 form-group">
                                <p>Pas encore inscrit ? <a href="register.php">Inscription</a></p>
                            </div>
                        </div>
                    </form>
                    <div id="success_message" style="width:100%; height:100%; display:none; "> <h3>Connexion validé !</h3> </div>
                    <div id="error_message" style="width:100%; height:100%; display:none; "> <h3>Erreur</h3> Désolé il y a des erreurs dans votre formulaire. </div>
                </div>
            </div>
        </div>
        </body>';
    

}

                        
$db->disconnect();
gk_cb_html_fin() ;


?>
