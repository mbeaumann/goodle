<?php
    session_start();
    function secureRandom($length = 5) {
        $str = bin2hex(openssl_random_pseudo_bytes($length));
        return strtoupper(substr(base_convert($str, 16, 35), 0, $length));
    }
    
    header('Content-Type: image/png');
    $captcha = imagecreatefromjpeg('../images/captcha.jpg');
    $captchaValue = secureRandom();
    $_SESSION['captchaVal'] = $captchaValue;

    $captchaArray = str_split($captchaValue);
    $font = '../font/doodletters.ttf';
    $posX = 24;
    $posY = 30;
    $size = 24;

    foreach ($captchaArray as $chara) {
        //$colorTxt = imagecolorallocate($captcha, rand(0, 255), rand(0, 255), rand(0, 255));
        $colorTxt = imagecolorallocate($captcha, 40, 40, 40);
        imagettftext($captcha, $size, rand(-50, 50), $posX, $posY, $colorTxt, $font, $chara);
        $posX += $size;
    }

    imagepng($captcha);
    imagedestroy($captcha);
?>
