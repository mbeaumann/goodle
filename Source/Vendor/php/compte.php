<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 12/10/2018
 * Time: 15:00
 */
    ob_start();

    require("./DB/Database.php");
    require("./Object/Account.php");
    require("./Manager/AccountManager.php");
    require("./bibli_cuiteur.php");
    

    use \UniTestor as UT;

    $title = "Goodle - Mon compte";
    $style = "../styles/general.css";


    session_start();



if(!isset($_SESSION['Id'])) {
        header('location: ../welcome.php');
        exit();
    }

    $Id = $_SESSION['Id'];
    $Name = $_SESSION['Name'];
    $Username = $_SESSION['Username'];
    $Email = $_SESSION['Email'];
    $Surname = $_SESSION['Surname'];
    $Password = $_SESSION['Password'];


    $db = new Database();
    $db->getConnection()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->getConnection()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->getConnection()->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $user = new UT\Account($Username, $Email, $Password, $Name, $Surname,'');
    $user_manager = new AccountManager($db->getConnection());


    gk_cb_html_debut($title, $style);
    affiche_nav_connected();




if(isset($_POST['btnMail'])) {
    $mail = trim($_POST['mail']);
    $confirmeMail = trim($_POST['confirmeMail']);
    $mdp = md5(trim($_POST['mdp']));

    $erreurMail = modificationMail($mail, $confirmeMail, $mdp, $user, $user_manager);
    $nbErrMail = count($erreurMail);

    if ($nbErrMail > 0) {
        echo '<div id="error">';
        echo '<strong>Les erreurs suivantes ont &eacute;t&eacute; d&eacute;tect&eacute;es</strong>';
        for ($i = 0; $i < $nbErrMail; $i++) {
            echo '<br>', $erreurMail[$i];
        }
        echo '</div>';
    }
    else {
        $user->setEmail($mail);
        $user->setId($Id);
        $user_manager->update($user);
        echo '<div id="message">',
                    '<p>Mail was updated correctly</p>',
                '</div>';
    }

    aff_form();

}
else if(isset($_POST['btnMdp'])) {
    $ancienMdp = md5(trim($_POST['ancienMdp']));
    $nouveauMdp = trim($_POST['nouveauMdp']);
    $confirmerMdp = trim($_POST['confirmerMdp']);

    $erreurMdp = modificationMdp($ancienMdp, $nouveauMdp, $confirmerMdp, $user);
    $nbErrMdp = count($erreurMdp);

    if ($nbErrMdp > 0) {
        echo '<div id="error">';
        echo '<strong>Les erreurs suivantes ont &eacute;t&eacute; d&eacute;tect&eacute;es</strong>';
        for ($i = 0; $i < $nbErrMdp; $i++) {
            echo '<br>', $erreurMdp[$i];
        }
        echo '</div>';
    }
    else {
        $user->setPassword(md5($nouveauMdp));
        $user->setId($Id);
        echo $user_manager->update($user);
        echo '<div id="message">',
                    '<p>Password was updated correctly</p>',
                '</div>';
    }
    aff_form();
}
else {


    aff_form();
}


function aff_form() {

    echo '<body >
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2>Mon compte</h2> 
                    <form role="form" method="post" id="reused_form">
                    <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="mail"> Nouvelle adresse email:</label>
                                <input type="text" class="form-control" id="mail" name="mail" maxlength="50">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="confirmeMail"> Confirmer votre adresse email:</label>
                                <input type="text" class="form-control" id="confirmeMail" name="confirmeMail" maxlength="50">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="mdp"> Mot de passe:</label>
                                <input type="password" class="form-control" id="mdp" name="mdp" maxlength="50">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <button type="submit" class="btn btn-lg btn-success btn-block" id="btnMail" name="btnMail">Changer l\'adresse email </button>
                            </div>
                        </div>			          
                    </form>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2>Mon compte</h2> 
                    <form role="form" method="post" id="reused_form">
                    <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="ancienMdp"> Ancien mot de passe :</label>
                                <input type="password" class="form-control" id="ancienMdp" name="ancienMdp" maxlength="50">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="nouveauMdp"> Nouveau mot de passe :</label>
                                <input type="password" class="form-control" id="nouveauMdp" name="nouveauMdp" maxlength="50">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="confirmerMdp"> Confirmer votre mot de passe:</label>
                                <input type="password" class="form-control" id="confirmerMdp" name="confirmerMdp" maxlength="50">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <button type="submit" class="btn btn-lg btn-success btn-block" id="btnMdp" name="btnMdp">Changer le mot de passe </button>
                            </div>
                        </div>			          
                    </form>
                </div>
            </div>
        </div>
        </body>';


}

function modificationMail($email, $confirmeMail, $mdp, $user) {
    $errors = array(); 
    $mailVal = array(); 

    //Verify elder password
    if ($user->getPassword() != $mdp){
        array_push($errors, "Password incorrect");
    }
    // Vérification du mail
    if ($email != $confirmeMail) {
        array_push($errors, 'The two mail address do not match');
    }
    if ($email == '' || $confirmeMail == '') {
        array_push($errors, 'The mail address is empty');
    }
    //Mail validations
    if (!preg_match('`[a-z]`',$confirmeMail)){
      array_push($mailVal, "Email must contain at least one lowercase letter");
  }
  if (!preg_match('`[A-Z]`',$confirmeMail)){
      array_push($mailVal, "Email must contain at least one uppercase letter");
  }
  if (!preg_match('`[0-9]`',$confirmeMail)){
      array_push($mailVal, "Email must contain at least  at least one number");
  }
  if (!preg_match('#[\\~\\`\\!\\#\\$\\%\\^\\&\\*\\(\\)\\_\\-\\+\\=\\{\\}\\[\\]\\|\\:\\;\\&lt;\\&gt;\\.\\?\\/\\\\\\\\]+#',$confirmeMail)){
      array_push($mailVal, "Email must contain at least  at least one special character");
  }
  if (preg_match('/^\\./', $email) or preg_match('/\\.$/', $confirmeMail)){
    array_push($errors, "Email can not start or finish with point"); 
  }
  if (preg_match('/(\\.)\\1{1}/', $confirmeMail)){
    array_push($errors, "Email can not contain two or more consecutive points"); 
  }
  if (!preg_match("/^([a-zA-Z0-9\+_\-]+)(\.[a-zA-Z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $confirmeMail)){
    array_push($errors, "Invalid format mail");
  }

  if (preg_match('/\\.@/', $email) or preg_match('/\\.$/', $confirmeMail)){
    array_push($errors, "Email can not contain the string .@"); 
  }
  if (!preg_match('`[\\.]+`', $email) or preg_match('/\\.$/', $confirmeMail)){
    array_push($errors, "Email must contain at least one point"); 
  }
  if (count($mailVal) == 4){
    array_push($errors, "Error. Please introduce a valid mail");
  }

    return $errors;
}

function modificationMdp($ancienmdp, $nouveauMdp, $confirmerMdp, $user) {

    $erreurs = array();

    //Verify elder password
    if ($user->getPassword() != $ancienmdp){
        array_push($erreurs, "Incorrect current password");
    }
    // Vérification du passe
    if ($nouveauMdp != $confirmerMdp) {
        array_push($erreurs, 'The two new passwords do not match');
    }
    //Password validations
    if (strlen($nouveauMdp) < 8){
        array_push($erreurs, "New password must contain at least 8 characters");
    }
    if (!preg_match('`[a-z]`',$nouveauMdp)){
        array_push($erreurs, "New password must contain at least one lowercase letter");
    }
    if (!preg_match('`[A-Z]`',$nouveauMdp)){
        array_push($erreurs, "New password must contain at least one uppercase letter");
    }
    if (!preg_match('`[0-9]`',$nouveauMdp)){
        array_push($erreurs, "New password must contain at least one number");
    }
    if (!preg_match('#[\\~\\`\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\_\\-\\+\\=\\{\\}\\[\\]\\|\\:\\;\\&lt;\\&gt;\\.\\?\\/\\\\\\\\]+#',$nouveauMdp)){
        array_push($erreurs, "New password must contain at least  at least one special character");
    }

    return $erreurs;
}



gk_cb_html_fin();



?>