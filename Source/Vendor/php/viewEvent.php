<?php

ob_start();

require("./DB/Database.php");
require("./Object/Account.php");
require("./Object/Event.php");
require("./Object/Proposal.php");
require("./Manager/AccountManager.php");
require("./Manager/EventManager.php");
require("./Manager/Account_ProposalManager.php");
require("./Manager/ProposalManager.php");
require("./bibli_cuiteur.php");

$title = "Goodle - Event";
$style = "./../styles/test.css";

session_start();

if(!isset($_SESSION['Id'])) {
    header('location: ../../welcome.php');
    exit();
}

$Id = $_SESSION['Id'];
$Name = $_SESSION['Name'];
$Username = $_SESSION['Username'];
$Email = $_SESSION['Email'];
$Surname = $_SESSION['Surname'];
$Password = $_SESSION['Password'];

gk_cb_html_debut($title, $style);
affiche_nav_connected();

use \UniTestor as UT;

$db = new Database();

$user = new UT\Account($Username, $Email, $Password, $Name, $Surname,'');
$user->setId($Id);

$event_manager = new EventManager($db->getConnection());

if(!isset($_POST['btnSuppression']) && !isset($_POST['btnValidation'])) {
    view_event($Id, $event_manager, $_GET['id']);
}

if(isset($_POST['btnSuppression'])) {
    delete_event($event_manager, $_GET['id'], $db);
}

if(isset($_POST['btnValidation'])) {
    $date = trim($_POST['dateEvent']);
    eventChooseDate($event_manager,$user->getId(),$_GET['id'],$date,$db);
    //send_mail();
    header("location: accueil.php");
}

function view_event($userId, $event_manager, $eventId) {
    $result = $event_manager->getEventAndProposal($eventId, $userId);
    $row = $result->fetch(PDO::FETCH_ASSOC);
    $event = new UT\Event('','','','');
    if($result->rowCount() == 0) {
        header("location: ../../welcome.php");
        exit();
    }
    
    $resultAll = $event_manager->getEventAndProposal($eventId, $userId);
    $rowAll = $resultAll->fetchAll(PDO::FETCH_ASSOC);
    $proposal = new UT\Proposal('','');
    if($resultAll->rowCount() == 0) {
        header("location: ../../welcome.php");
        exit();
    }
    
    $event->hydrate($row);
    
    echo    '<div class="container">
                <h2>Evénement - '.$event->getName().'</h2>';
    echo            '<label> Description : </label>';
    echo            '<p>'.$event->getName().'</p>';
    echo            '<label> Fin du sondage : </label>';
    echo            '<p>'.$event->getFinSondage().'</p>';
    echo            '<label> Proposition : </label>';
    foreach($rowAll as $r) {
        $proposal->hydrate($r);
        echo '<p>'.$proposal->getDate().'</p>';
    }
    echo    '<form role="form" method="post" id="reused_form">
                <div class="row">
                    <div class="col-sm-2 form-group">
                        <button type="submit" class="btn btn-lg btn-success btn-block" id="btnSuppression" name="btnSuppression">Suppression </button>
                    </div>
                </div>
            </form>';
    echo    '<form role="form" method="post" id="reused_form">
                <label> Choix de la date de l\'événement : </label>
                <select name="dateEvent" size="1">';
    foreach($rowAll as $r) {
        $proposal->hydrate($r);
        echo '<option>'.$proposal->getDate().'</option>';
    }
    echo        '</select>
                <div class="row">
                    <div class="col-sm-2 form-group">
                        <button type="submit" class="btn btn-lg btn-success btn-block" id="btnValidation" name="btnValidation">Validation </button>
                    </div>
                </div>
            </form>';
    echo    '</div>';
}

function delete_event($event_manager, $eventId, $db) {
    $account_proposal_manager = new Account_ProposalManager($db->getConnection());
    $proposal_manager = new ProposalManager($db->getConnection());
    
    $result_event_manager = $event_manager->deleteEvent($eventId);
    $result_account_proposal_manager = $account_proposal_manager->deleteAccountProposal($eventId);
    $result_proposal_manager = $proposal_manager->deleteProposal($eventId);
    
    header('location: accueil.php');
    exit();
}

function eventChooseDate($event_manager,$accountId, $eventId, $response, $db) {
    $result_event_manager = $event_manager->chooseEventDate($accountId,$eventId,$response);
}

function send_mail() {
    $mail = 'anthony.wetzel02@edu.univ-fcomte.fr'; // Déclaration de l'adresse de destination.
    if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
    {
        $passage_ligne = "\r\n";
    }
    else
    {
        $passage_ligne = "\n";
    }
    //=====Déclaration des messages au format texte et au format HTML.
    $message_txt = "Salut à tous, voici un e-mail envoyé par un script PHP.";
    $message_html = "<html><head></head><body><b>Salut à tous</b>, voici un e-mail envoyé par un <i>script PHP</i>.</body></html>";
    //==========
    
    //=====Création de la boundary
    $boundary = "-----=".md5(rand());
    //==========
    
    //=====Définition du sujet.
    $sujet = "Hey mon ami !";
    //=========
    
    //=====Création du header de l'e-mail.
    $header = "From: \"WeaponsB\"<weaponsb@mail.fr>".$passage_ligne;
    $header.= "Reply-to: \"WeaponsB\" <weaponsb@mail.fr>".$passage_ligne;
    $header.= "MIME-Version: 1.0".$passage_ligne;
    $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
    //==========
    
    //=====Création du message.
    $message = $passage_ligne."--".$boundary.$passage_ligne;
    //=====Ajout du message au format texte.
    $message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_txt.$passage_ligne;
    //==========
    $message.= $passage_ligne."--".$boundary.$passage_ligne;
    //=====Ajout du message au format HTML
    $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
    $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
    $message.= $passage_ligne.$message_html.$passage_ligne;
    //==========
    $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
    $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
    //==========
    
    //=====Envoi de l'e-mail.
    mail($mail,$sujet,$message,$header);
    //==========
}

$bd = null;
gk_cb_html_fin() ;

?>