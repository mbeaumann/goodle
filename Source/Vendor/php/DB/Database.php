<?php
/**
 * Created by IntelliJ IDEA.
 * User: coren
 * Date: 04/10/2018
 * Time: 15:36
 */


class Database
{

    private $_connection;
    private static $_instance; //The single instance

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance() {
        if(!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    // Constructor
    public function __construct() {
        $this->_connection = new PDO('mysql:host=m2gl.deptinfo-st.univ-fcomte.fr;dbname=m2test3', 'm2test3', 'm2test3');

        // Error handling
        if(mysqli_connect_error()) {
            trigger_error("Failed to conencto to MySQL: " . mysql_connect_error(),
                E_USER_ERROR);
        }
    }
    // Magic method clone is empty to prevent duplication of connection
    private function __clone() { }
    // Get mysqli connection
    public function getConnection() {
        return $this->_connection;
    }


    public function disconnect()
    {
        $this->_connection = null;
    }



}


