
<?php
ob_start();

require("./bibli_cuiteur.php");
require("./bibli_generale.php");
require("./DB/Database.php");

require("./Object/Account.php");
require("./Object/Proposal.php");
require("./Object/Event.php");

require("./Manager/EventManager.php");
require("./Manager/ProposalManager.php");

$title = "Goodle - Création événement";
$style = "./../styles/general.css";

use \UniTestor as UT;


session_start();

gk_cb_html_debut($title, $style);
affiche_nav_connected();

if(!isset($_POST['btnSoumettre'])) {
    aff_form();
}


$Id = $_SESSION['Id'];
$Name = $_SESSION['Name'];
$Username = $_SESSION['Username'];
$Email = $_SESSION['Email'];
$Surname = $_SESSION['Surname'];
$Password = $_SESSION['Password'];


$db = new Database();
$db->getConnection()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->getConnection()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
$db->getConnection()->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

$user = new UT\Account($Username, $Email, $Password, $Name, $Surname,'');
$user->setId($Id);


if(isset($_POST['btnSoumettre'])) {
    $nameEvent = trim($_POST['nameEvent']);
    $Description = trim($_POST['description']);
    $Owner = $user->getId();
    $proposalDate = $_POST['proposal'];
    $finSondage = $_POST['finSondage'];


    $erreur = verification($nameEvent, $Description, $proposalDate, $finSondage);

    //verification proposal
    foreach($proposalDate as $value) {
        if ($value == '') continue;
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$value))
        {
            array_push($erreur, "Proposal Format Invalid. It must be YYYY-MM-DD");
        }
    }

    $count = count($erreur);

    if($count != 0  ) {
        $erreur[] = "Problèmes";
    }

    if (count($erreur) == 0) {

        $event = new UT\Event($Owner, $nameEvent, $Description, $finSondage);
        $event_manager = new EventManager($db->getConnection());
        $IdEvent = $event_manager->add($event);

        $proposalIdArray = array();
        $proposalObjectArray = array();

        //verification proposal
        foreach($proposalDate as $value) {
            if ($value == '') continue;
            $proposal = new UT\Proposal('','');
            $proposal->setDate($value);
            $proposal->setFkeyEvent($IdEvent);
            $proposal_manager = new ProposalManager($db->getConnection());
            $IdProposal = $proposal_manager->add($proposal);

            array_push($proposalIdArray, $IdProposal);
            array_push($proposalObjectArray, $proposal);

        }



        header ('location: accueil.php');
        exit();
    }

    else {

        echo '<h3>', $erreur[0], '</h5>';

        $_POST['nameEvent'] = '';
        $_POST['description'] = '';
        $_POST['finSondage'] = '';
        $_POST['proposal'] = '';

        echo '</br>';
        echo aff_form();

    }

}



function aff_form()
{
    echo '<body >
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2>Formulaire de l\'événement</h2> 
                    <form id="formEvent" role="form" method="post" id="reused_form">
                    <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="nameEvent"> Nom de l\'événement:</label>
                                <input type="text" class="form-control" id="nameEvent" name="nameEvent" maxlength="50">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="description">Descriptif:</label>
                                <input type="text" class="form-control" id="description" name="description" maxlength="100">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="description">Date de fin du sondage:</label>
                                <input type="text" class="form-control" name="finSondage">                           
                            </div>
                        </div>
                        
                        <div class="row" id="divToMultiple">
			    <div class="col-sm-10">
                            	<label for="description">Proposition:</label>
			    </div>
                            <div class="col-sm-10 form-group">
                                <input type="text" class="form-control" name="proposal[]" id="proposal" onkeyup="addInput()">    
                            </div>
			    <div class="col-sm-1 form-group">
				<button type="button" class="btn btn-default addButton"><i class="fa fa-plus">+</i></button> 
			    </div>
                        </div>
                        
			<div class="row" id="divToMultiple">
                        	<div class="form-group hide" id="bookTemplate">
		                        <div class="col-sm-10 form-group">
		                            <input type="text" class="form-control" name="proposal[]" id="proposal" onkeyup="addInput()">    
		                        </div>
					<div class="col-sm-1 form-group">
						<button type="button" class="btn btn-default removeButton"><i class="fa fa-minus">-</i></button>
					</div>
                            	</div>
                        </div>
                        
                        <script type="text/javascript">
                        var counter = 0;
                        $(\'#formEvent\')
                            // Add button click handler
                            .on(\'click\', \'.addButton\', function() {
                                counter++;
                                var $template = $(\'#bookTemplate\'),
                                    $clone    = $template
                                                    .clone()
                                                    .removeClass(\'hide\')
                                                    .removeAttr(\'id\')
                                                    .attr(\'data-book-index\', counter)
                                                    .insertBefore($template);
                        
                                // Update the name attributes
                                $clone
                                    .find(\'[name="proposal[]"]\').attr(\'name\', \'proposal[]\').end();
                            })
                        
                            // Remove button click handler
                            .on(\'click\', \'.removeButton\', function() {
                                var $row  = $(this).parents(\'.form-group\'),
                                    index = $row.attr(\'data-book-index\');
                        
                                // Remove element containing the fields
                                $row.remove();
                            });
                        </script>
                        
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <button type="submit" class="btn btn-lg btn-success btn-block" id="btnSoumettre" name="btnSoumettre">Soumettre </button>
                            </div>
                        </div>
			            
                    </form>
                    <div id="success_message" style="width:100%; height:100%; display:none; "> <h3>Connexion validé !</h3> </div>
                    <div id="error_message" style="width:100%; height:100%; display:none; "> <h3>Erreur</h3> Désolé il y a des erreurs dans votre formulaire. </div>
                </div>
            </div>
        </div>
        </body>';
}


function verification($nameEvent, $description, $proposal, $finSondage) {
    $errors = array();

    if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$finSondage))
    {
        array_push($errors, "Fin Sondage Format Invalid. It must be YYYY-MM-DD");
    }
    if ($nameEvent == '' && $description == '') {
        array_push($errors, "Not empty name or descritpion");
    }

    return $errors;
}



$bd = null;
gk_cb_html_fin() ;



?>


