<?php

ob_start();

require("./DB/Database.php");
require("./Object/Account.php");
require("./Object/Event.php");
require("./Manager/AccountManager.php");
require("./Manager/EventManager.php");
require("./bibli_cuiteur.php");

$title = "Goodle - Accueil";
$style = "";

session_start();

if(!isset($_SESSION['Id'])) {
    header('location: ../../welcome.php');
    exit();
}

$Id = $_SESSION['Id'];
$Name = $_SESSION['Name'];
$Username = $_SESSION['Username'];
$Email = $_SESSION['Email'];
$Surname = $_SESSION['Surname'];
$Password = $_SESSION['Password'];

gk_cb_html_debut($title, $style);
affiche_nav_connected();

/*echo '
<body >
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2>Goodle</h2> 
                    <form role="form" method="post" id="reused_form">
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <a href="createEvent.php">Créer un evenement</a>
                            </div>
			                <div class="col-sm-6 form-group">
                                <a href="connexion.php">Connexion</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>';*/
echo '
<body >
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h2>Bienvenue sur Goodle !</h2>
                </div>
            </div>
        </div>';

use \UniTestor as UT;

$db = new Database();


$user = new UT\Account($Username, $Email, $Password, $Name, $Surname,'');

$event_manager = new EventManager($db->getConnection());

aff_event($Id, $event_manager);

aff_proposal($Id, $event_manager);



function aff_event($userId, $event_manager) {
    echo '
    <div class="container">
  <h2>Mes événements (Créateur)</h2>
  <table class="table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>';
    $result = $event_manager->getEventOwner($userId);

    
    $row = $result->fetchAll(PDO::FETCH_ASSOC);
    $event = new UT\Event('','','','');
    
    foreach($row as $r) {
        $event->hydrate($r);
        echo    "<tr onclick=\"window.location='viewEvent.php?id=".$event->getId()."';\">";
        echo        '<td>'.$event->getName().'</td>';
        echo        '<td>'.$event->getDescription().'</td>';
        echo    '</tr>';
    }

    echo '</tbody></table></div>';
}



function aff_proposal($userId, $event_manager) {
    echo '
    <div class="container">
  <h2>Mes propositions (Invité)</h2>
  <table class="table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>';
    $result = $event_manager->getEventFromProposal($userId);


    $row = $result->fetchAll(PDO::FETCH_ASSOC);
    $event = new UT\Event('','','','');

    foreach($row as $r) {
        $event->hydrate($r);
        echo    "<tr onclick=\"window.location='viewProposal.php?id=".$event->getId()."';\">";
        echo        '<td>'.$event->getName().'</td>';
        echo        '<td>'.$event->getDescription().'</td>';
        echo    '</tr>';
    }

    echo '</tbody></table></div>';
}


$bd = null;
gk_cb_html_fin() ;


?>