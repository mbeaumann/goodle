<?php
session_start();

//initializing DB
require("./DB/Database.php");
require("./Object/Account.php");
require("./Manager/AccountManager.php");
require("./bibli_cuiteur.php");

$title = "Goodle - Register";
$style = "";

gk_cb_html_debut($title, $style);
affiche_nav_no_connected();

use \UniTestor as UT;

// initializing variables
$username = "";
$email    = "";
$name    = "";
$surname = "";
$errors = array(); 
$mailVal = array(); 
$password_1 = "";
$password_2 = "";
$bday = date("Y-m-d");
$actualdate = date("Y-m-d");

// connect to the database
//$db = mysqli_connect('m2gl.deptinfo-st.univ-fcomte.fr', 'm2test3', 'm2test3', 'm2test3');
$db = new Database();

  // REGISTER USER
  if (isset($_POST['reg_user'])) {
  
  // receive all input values from the form
  $username = trim($_POST['username']);
  $email = trim($_POST['email']);
  $password_1 = trim($_POST['password_1']);
  $password_2 = trim($_POST['password_2']);
  $name = trim($_POST['name']);
  $surname = trim($_POST['surname']);
  $bday = trim($_POST['bday']);
  /*
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);
  $name = mysqli_real_escape_string($db, $_POST['name']);
  $surname = mysqli_real_escape_string($db, $_POST['surname']);
  $bday = mysqli_real_escape_string($db, $_POST['bday']);
  //$captcha = mysqli_real_escape_string($db, $_POST['captcha']);
  */

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if (empty($name)) { array_push($errors, "Name is required"); }
  if (empty($surname)) { array_push($errors, "Surname is required"); }
  if (empty($bday)) { array_push($errors, "Birthdate is required"); }
  if ($password_1 != $password_2) {
  array_push($errors, "The two passwords do not match");
  }

  //User validations
  if (strlen($username) < 3){
      array_push($errors, "Username must contain at least 3 characters");
  }

  if (strlen($username) > 255){
      array_push($errors, "Username must contain maximum 255 characters!");
  }
  //Mail validations
  if (!preg_match('`[a-z]`',$email)){
      array_push($mailVal, "Email must contain at least one lowercase letter");
  }
  if (!preg_match('`[A-Z]`',$email)){
      array_push($mailVal, "Email must contain at least one uppercase letter");
  }
  if (!preg_match('`[0-9]`',$email)){
      array_push($mailVal, "Email must contain at least  at least one number");
  }
  if (!preg_match('#[\\~\\`\\!\\#\\$\\%\\^\\&\\*\\(\\)\\_\\-\\+\\=\\{\\}\\[\\]\\|\\:\\;\\&lt;\\&gt;\\.\\?\\/\\\\\\\\]+#',$email)){
      array_push($mailVal, "Email must contain at least  at least one special character");
  }
  /*if (!preg_match('#[\\~\\`\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\_\\-\\+\\=\\{\\}\\[\\]\\|\\:\\;\\&lt;\\&gt;\\.\\?\\/\\\\\\\\]+#',$email)){
      array_push($mailVal, "Email must contain at least  at least one special character");
  }*/
  if (preg_match('/^\\./', $email) or preg_match('/\\.$/', $email)){
    array_push($errors, "Email can not start or finish with point"); 
  }
  if (preg_match('/(\\.)\\1{1}/', $email)){
    array_push($errors, "Email can not contain two or more consecutive points"); 
  }
  if (!preg_match("/^([a-zA-Z0-9\+_\-]+)(\.[a-zA-Z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)){
    array_push($errors, "Invalid format mail");
  }

  if (preg_match('/\\.@/', $email) or preg_match('/\\.$/', $email)){
    array_push($errors, "Email can not contain the string .@"); 
  }
  if (!preg_match('`[\\.]+`', $email) or preg_match('/\\.$/', $email)){
    array_push($errors, "Email must contain at least one point"); 
  }
  if (count($mailVal) == 4){
    array_push($errors, "Error. Please introduce a valid mail");
  }

  //Password validations
  if (strlen($password_1) < 8){
      array_push($errors, "Password must contain at least 8 characters");
  }
  if (!preg_match('`[a-z]`',$password_1)){
      array_push($errors, "Password must contain at least one lowercase letter");
  }
  if (!preg_match('`[A-Z]`',$password_1)){
      array_push($errors, "Password must contain at least one uppercase letter");
  }
  if (!preg_match('`[0-9]`',$password_1)){
      array_push($errors, "Password must contain at least one number");
  }
  if (!preg_match('#[^a-z0-9]+#i', $password_1)){
    array_push($errors, "Password must contain at least  at least one special character");
  }
  /*if (!preg_match('#[\\~\\`\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\_\\-\\+\\=\\{\\}\\[\\]\\|\\:\\;\\&lt;\\&gt;\\.\\?\\/\\\\\\\\]+#',$password_1)){
      array_push($errors, "Password must contain at least  at least one special character");
  }*/
  
  //Date validations

      if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$bday))
      {
          array_push($errors, "Birthdate Format Invalid. It must be YYYY-MM-DD");
      }
  if ($bday > $actualdate){
      array_push($errors, "Birthdate must be less than current date");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  //$user_check_query = "SELECT * FROM ACCOUNT WHERE Username='$username' OR Email='$email' LIMIT 1";
  //$result = mysqli_query($db, $user_check_query);
  //$user = mysqli_fetch_assoc($result);

  //User and email validation
  $user_acc = new UT\Account($username, $email,'','','','');
  $user_manager = new AccountManager($db->getConnection());
  $user_ck = $user_manager->user_check($user_acc);
  $email_ck = $user_manager->email_check($user_acc);
  if ($user_ck > 0){
    array_push($errors, "Username already exists");
  }
  if ($email_ck > 0){
    array_push($errors, "Email already exists");
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
    $password = md5($password_1);//encrypt the password before saving in the database

    /*
    $query = "INSERT INTO ACCOUNT (Username, Email, Password, Name, Surname, Birthdate) 
          VALUES('$username', '$email', '$password', '$name', '$surname', '$bday')";
    mysqli_query($db, $query);
    */
    $user_ins = new UT\Account($username, $email, $password, $name, $surname, $bday);
    $user_manager_ins = new AccountManager($db->getConnection());
    $inscription = $user_manager->add($user_ins);

    if($inscription_res != null){
      array_push($errors, "There is some problem in connection: " . $inscription_res );
    }
    $_SESSION['username'] = $username;
    $_SESSION['success'] = "You are now logged in";
    header('location: connexion.php');
  }

}
