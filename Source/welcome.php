
<?php

ob_start();

require("Vendor/php/bibli_cuiteur.php");
require("Vendor/php/bibli_generale.php");

$title = "Goodle - Accueil";
$style = "./Vendor/styles/general.css";
$bd = gk_cb_bd_connection();


gk_cb_html_debut($title, $style);

echo '<body >
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2>Accueil Goodle</h2> 
                    <form role="form" method="post" id="reused_form">
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <a href="Vendor/php/register.php">Inscription</a>
                            </div>
			                <div class="col-sm-6 form-group">
                                <a href="Vendor/php/connexion.php">Connexion</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>';


$bd = null;
gk_cb_html_fin() ;


?>


